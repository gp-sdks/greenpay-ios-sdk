# ****GreenPay iOS SDK Changelog****

## Changelog

***Version 0.0.1 - March 8, 2019***

* Initial release of the SDK.
* [Feature]: Checkout an already created order in GreenPay with a regular card.
* [Feature]: Tokenize a card in GreenPay.
* [Feature]: Checkout an already created order in GreenPay with a tokenized card.

***Version 0.0.2 - August 4, 2019.***

* Updated source code and Cocoapods dependencies to support Xcode 10.2.1 and Swift 5.

***Version 0.0.3 - November 4, 2019.***

* Updated source code and Cocoapods dependencies to support Xcode 11 and Swift 5.1.2.
* Updated CryptoSwift dependency from 1.0.0 to 1.1.3 to support Swfit 5.

***Version 0.0.3 - November 4, 2019.***

* [Feature]: Integrates checkout with the Kount service.
* [Feature]: Implements a collectForSession method to collect data for Kount service without checking out.
