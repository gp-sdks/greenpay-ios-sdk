# **GreenPay iOS SDK**

GreenPay is a payments platform that allows wesites, desktop and mobile apps to process payments.

In order to process payments you must create an account on [GreenPay](https://www.greenpay.me/).

## Installation

The iOS SDK is published as a [Cocoapod](https://cocoapods.org/). To install, simply add this line to your pod file:

```bash
pod 'GreenPay'
```

## Usage

First, you need to create a GreenPay ```GreenPayConfig``` object to configure the SDK with your account information. This information will be provided by the GreenPay Team when you create your GreenPay Account.

Second, call the initGreenPaySDK method to initialize the SDK.

```swift
import GreenPaySDK

let greenPaySecret = "";
let greenPayMerchantId = "";
let greenPayTerminal = "";
let greenPayPublicKey = "";

let config: GreenPayConfig = GreenPayConfig(greenPaySecret: greenPaySecret, greenPayMerchantId: greenPayMerchantId, greenPayTerminal: greenPayTerminal, greenPayPublickKey: greenPayPublicKey);

GreenPay.shared.initGreenPaySDK(config: config);
```
Additionally, you can switch between Sandbox and Production environments with this call (By default the SDK uses the Production environment).

```swift
GreenPay.shared.enableSandboxMode(sandboxModeEnabled: true);
```

## Making a Payment

First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingOrder```  with the Credit Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

```swift
var gpCreditCard : GreenPayCreditCard = GreenPayCreditCard();
gpCreditCard.cardHolder = "";
gpCreditCard.cardNumber = "";
gpCreditCard.cvc = "";
gpCreditCard.expirationMonth = 9;
gpCreditCard.expirationYear = 21;
gpCreditCard.nickname = "";

GreenPay.shared.checkoutExistingOrder(greenPayCreditCard: self.gpCreditCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in

}).catch({ (error) in
print("checkoutExistingOrder Error: ");
})
```
The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.

## Tokenizing a Card 

First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to tokenize a card. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingTokenizeCardOrder```  with the Credit Card to tokenize and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

```swift
GreenPay.shared.checkoutExistingTokenizeCardOrder(greenPayCreditCard: self.gpCreditCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayTokenizeCardCheckoutOrderResult) in

self.gpTokenizedCreditCard = GreenPayTokenizedCard();
self.gpTokenizedCreditCard!.token = greenPayTokenizeCardCheckoutOrderResult.token;

print("checkoutExistingTokenizeCardOrder Success: Token: " + greenPayTokenizeCardCheckoutOrderResult.token);

}).catch({ (error) in
print("checkoutExistingTokenizeCardOrder Error: ", error.localizedDescription);
})

}.catch { (error) in
print("createNewTokenizeCardGreenPayOrder Error: " , error.localizedDescription);
}
```
The returning object  ```GreenPayTokenizeCardCheckoutOrderResult``` will have the Card Token representing the Card in GreenPay.

## Making a Payment with a Tokenized Card

First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

Second, from your app you can call ```checkoutExistingOrder```  with the Tokenized Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

```swift
var gpTokenizedCreditCard : GreenPayTokenizedCard()
self.gpTokenizedCreditCard.token = "";

GreenPay.shared.checkoutExistingOrder(greenPayTokenizedCard: self.gpTokenizedCreditCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in

}).catch({ (error) in
print("checkoutExistingOrder Error: ");
})
```
The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.

## Collecting data for Kount service with a provided session id

In order to comply with the Greenpay service regulation, in case you have processed a checkout in the backend and you have to collect data in Kount using the provided session id. 

```swift
GreenPay.shared.collectForSession(sessionID: providedSessionID).done({ (sessionID) in
    print("Success collecting info for Kount service");
}).catch({ (error) in
    print("ERROR collection info for Kount service");
});
```
