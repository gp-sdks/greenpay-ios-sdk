//
//  GreenPay.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation
import PromiseKit

/// GreenPay iOS SDK main class entry point. Use the `shared` variable to access it.
public class GreenPay: NSObject {
    
    /// Singleton variable for using the GreenPay class.
    public static let shared = GreenPay()
    
    /// GreenPay SDK Configuration
    var greenPayConfig: GreenPayConfig? = nil;
    
    /// Whether the SDK functions in Sandbox mode or in Production Mode
    var sandboxModeEnabled: Bool = false;
    
    /// Whether logs are enabled or no;
    var enableLogs: Bool = false;
    
    // MARK: - Configuration
    
    /**
     Initializes the GreenPaySDK with a configuration.
     
     - Parameters:
        - config: The GreenPayConfgig object used for initializing the SDK.
     */
    public func initGreenPaySDK(config: GreenPayConfig){
        self.greenPayConfig = config;
        KDataCollector.shared().merchantID = 790000;
        KDataCollector.shared().locationCollectorConfig = KLocationCollectorConfig.requestPermission;
        KDataCollector.shared().environment = KEnvironment.production
    }
    
    /**
     Sets whether the SDK uses the SANDBOX or Production environment.
     Defaults to Production (False).
     
     - Parameters:
        - sandboxModeEnabled: True to enable the SDK in Sandbox mode, for production, use False.
     */
    public func enableSandboxMode(sandboxModeEnabled: Bool){
        self.sandboxModeEnabled = sandboxModeEnabled;
        GreenPayAPI.shared.setSandboxMode(sandboxModeEnabled: sandboxModeEnabled);
        KDataCollector.shared().environment = KEnvironment.test;
        if (self.sandboxModeEnabled){
            GreenPayLogger.logInfo(message: NSLocalizedString("SANDBOX_ENABLED", comment: ""))
        }
    }
    
    public func collectForSession(sessionID: String) -> Promise <String> {
        return Promise { seal in
            KDataCollector.shared().collect(forSession: sessionID) { (sessionID, success, error) in
                if success {
                    GreenPayLogger.logInfo(message: "KOUNT_RESULT - SUCCESS");
                    seal.fulfill(sessionID);
                } else {
                    if ((error) != nil) {
                        GreenPayLogger.logInfo(message: "KOUNT_RESULT - ERROR " + error!.localizedDescription);
                        seal.reject(GreenPayError.greenPayError(code: 1, message: error!.localizedDescription));
                    }
                }
            }
        }
        
    }
    
    /**
     Sets whether the SDK writes logs.
     Defaults to No logs (False).
     
     - Parameters:
        - enableLogs: True to write logs, use False to disable logs.
     */
    public func enableLogs(enableLogs: Bool){
        GreenPayLogger.logsEnabled = enableLogs;
    }
    
    /**
     Checks whether the SDK is ready to use or not.
     
     - Throws: `GreenPayError` when init has not been called.
     */
    private func checkGreenPaySDKInit() throws{
        if (self.greenPayConfig == nil){
            //throw GreenPayError(NSLocalizedString("ERROR_NO_CONFIG", comment: ""));
            throw GreenPayError.greenPayError(code: 1, message: NSLocalizedString("ERROR_NO_CONFIG", comment: ""));
        }
        
        try self.checkConfiguration();
    }
    
    /**
     Checks whether the SDK Configuration is set or not.
     
     - Throws: `GreenPayError` when config is not set.
     */
    private func checkConfiguration() throws{
        if (self.greenPayConfig?.greenPaySecret == nil || self.greenPayConfig?.greenPaySecret == ""){
            throw GreenPayError.configurationError(code: 1, message: NSLocalizedString("ERROR_CONFIG_INVALID_SECRET", comment: ""));
        }
        
        if (self.greenPayConfig?.greenPayMerchantId == nil || self.greenPayConfig?.greenPayMerchantId == ""){
            throw GreenPayError.configurationError(code: 1, message: NSLocalizedString("ERROR_CONFIG_INVALID_MERCHANT_ID", comment: ""));
        }
        
        if (self.greenPayConfig?.greenPayTerminal == nil || self.greenPayConfig?.greenPayTerminal == ""){
            throw GreenPayError.configurationError(code: 1, message:NSLocalizedString("ERROR_CONFIG_INVALID_TERMINAL", comment: ""));
        }
        
        if (self.greenPayConfig?.greenPayPublicKey == nil || self.greenPayConfig?.greenPayPublicKey == ""){
            throw GreenPayError.configurationError(code: 1, message:NSLocalizedString("ERROR_CONFIG_INVALID_PUBLICK_KEY", comment: ""));
        }
    }
    
    //MARK: - Checkout Orders (Make Payments)
    
    
    /**
     Creates a new GreenPay Order.
     
     - Parameters:
        - greenPayOrderInfo: A `GreenPayOrderInfo` object containing the information of the order.
     
     - Returns: A Promise with a `GreenPayOrderResult` containing the result of the Create Order Process.
     */
    public func createNewGreenPayOrder(greenPayOrderInfo: GreenPayOrderInfo) -> Promise<GreenPayOrderResult>{
        
        return Promise {
            seal in
        
            do{
                try self.checkGreenPaySDKInit();
            }catch{
                seal.reject(error)
            }
            
            let orderRequestData: API.OrderRequestData = API.OrderRequestData();
            orderRequestData.amount = greenPayOrderInfo.amount;
            orderRequestData.currency = greenPayOrderInfo.currency;
            orderRequestData.description = greenPayOrderInfo.description;
            orderRequestData.orderReference = greenPayOrderInfo.orderReference;
            orderRequestData.merchantId = (greenPayConfig?.greenPayMerchantId)!;
            orderRequestData.secret = (greenPayConfig?.greenPaySecret)!;
            orderRequestData.terminal = (greenPayConfig?.greenPayTerminal)!;
            
            GreenPayAPI.shared.createOrder(orderRequestData: orderRequestData).done { (orderResponseData) in
                
                var orderResult = GreenPayOrderResult();
            
                var securityInfo: GreenPayOrderSecurityInfo = GreenPayOrderSecurityInfo();
                securityInfo.session = orderResponseData.session;
                securityInfo.token = orderResponseData.token;
            
                orderResult.securityInfo = securityInfo;
                orderResult.orderInfo = greenPayOrderInfo;
                
                seal.fulfill(orderResult);
                
                }.catch { (Error) in
                    seal.reject(GreenPayError.greenPayError(code: 1, message: Error.localizedDescription));
                }
            
        }
    }
    
    /**
     Checkout (Make Payment) of a already created order.
     
     - Parameters:
        - greenPayCreditCard: A `GreenPayCreditCard` object with the credit card information.
        - greenPayOrderResult: A `GreenPayOrderResult` object the result of the Create Order Process.
     
     - Returns: A Promise with a `GreenPayCheckoutOrderResult` containing the result of the Checkout Order Process.
     */
    public func checkoutExistingOrder(greenPayCreditCard: GreenPayCreditCard, greenPayOrderResult: GreenPayOrderResult) -> Promise<GreenPayCheckoutOrderResult> {
        
        return Promise { seal in
            
            do{
                try self.checkGreenPaySDKInit();
            }catch{
                seal.reject(error)
            }
            
            let cardData : API.CardData = API.CardData();
            var cardInfo: API.CardInfo = API.CardInfo();
            var cardExpirationDate: API.CardExpirationDate = API.CardExpirationDate();
            
            cardExpirationDate.month = greenPayCreditCard.expirationMonth;
            cardExpirationDate.year = greenPayCreditCard.expirationYear;
            cardInfo.cardExpirationDate = cardExpirationDate;
            cardInfo.cardHolder = greenPayCreditCard.cardHolder;
            cardInfo.cardNumber = greenPayCreditCard.cardNumber;
            cardInfo.cvc = greenPayCreditCard.cvc;
            cardInfo.nickname = greenPayCreditCard.nickname;
            
            cardData.cardInfo = cardInfo;
            
            checkoutOrder(creditCard: cardData, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in
                
                seal.fulfill(greenPayCheckoutOrderResult);
            }).catch({ (error) in
                
                seal.reject(error);
            })
        }
    }
    
    /**
     Checkout (Make Payment) of a already created order with a Tokenized Card.
     
     - Parameters:
        - greenPayTokenizedCard: A `GreenPayTokenizedCard` object with the tokenized card.
        - greenPayOrderResult: A `GreenPayOrderResult` object the result of the Create Order Process.
     
     - Returns: A Promise with a `GreenPayCheckoutOrderResult` containing the result of the Checkout Order Process.
     */
    public func checkoutExistingOrder(greenPayTokenizedCard: GreenPayTokenizedCard, greenPayOrderResult: GreenPayOrderResult) -> Promise<GreenPayCheckoutOrderResult> {
        
        return Promise {
            seal in
            
            do{
                try self.checkGreenPaySDKInit();
            }catch{
                seal.reject(error)
            }
            
            let tokenizedCard : API.TokenizedCard = API.TokenizedCard();
            tokenizedCard.token = greenPayTokenizedCard.token;
            
            checkoutOrder(creditCard: tokenizedCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in
                
                seal.fulfill(greenPayCheckoutOrderResult);
                
            }).catch({ (error) in
                seal.reject(error);
            })
        }
    }
    
    /**
     Checkouts an GreenPay Order.
     
     - Parameters:
        - creditCard: A `API.CreditCard` whether it's a Credit Card Information or a Tokenized Credit Card.
        - greenPayOrderResult: A `GreenPayOrderResult` object the result of the Create Order Process.
     
     - Returns: A Promise with a `GreenPayCheckoutOrderResult` containing the result of the Checkout Order Process.
     */
    private func checkoutOrder(creditCard: API.CreditCard, greenPayOrderResult: GreenPayOrderResult) -> Promise<GreenPayCheckoutOrderResult> {
        
        return Promise { seal in
            let sessionID = KountUtils.shared.formatSessionID(sessionID: greenPayOrderResult.securityInfo.session);
            collectForSession(sessionID: sessionID).done { (sessionID) in
                
                let orderResponseData : API.OrderResponseData = API.OrderResponseData();
                orderResponseData.session = greenPayOrderResult.securityInfo.session;
                orderResponseData.token = greenPayOrderResult.securityInfo.token;
                
                let checkoutRequestData : API.CheckoutRequestData = try self.createCheckoutRequestData (creditCard: creditCard, orderResponseData: orderResponseData);
                
                GreenPayAPI.shared.checkoutOrder(orderResponseData: orderResponseData, checkoutRequestData: checkoutRequestData).done { (checkoutResponseData) in
                    
                    let transactionVerified = GreenPayProcess.verifyCheckoutResponse(checkoutResponseData: checkoutResponseData, greenPayPublicKey: (self.greenPayConfig?.greenPayPublicKey)!);
                    if (!transactionVerified) {
                        seal.reject(GreenPayError.greenPayError(code: 1, message: NSLocalizedString("ERROR_TRANSACTION_NOT_VALIDATED", comment: "")));
                    }
                    
                    var greenPayCheckoutOrderResult : GreenPayCheckoutOrderResult = GreenPayCheckoutOrderResult();
                    greenPayCheckoutOrderResult.authorization = checkoutResponseData.authorization;
                    greenPayCheckoutOrderResult.brand = checkoutResponseData.brand;
                    greenPayCheckoutOrderResult.last4 = checkoutResponseData.last4;
                    greenPayCheckoutOrderResult.orderId = checkoutResponseData.orderId;
                    greenPayCheckoutOrderResult.signature = checkoutResponseData.signature;
                    greenPayCheckoutOrderResult.status = checkoutResponseData.status;
                    
                    seal.fulfill(greenPayCheckoutOrderResult);
                    
                    }.catch { (error) in
                        seal.reject(error);
                    }
                
            }.catch({(error) in
                seal.reject(error);
            });
            
            
        }
    }
    
    /**
     Creates a `API.CheckoutRequestData` object to call directly the Checkout Order Process
     
     - Parameters:
        - creditCard: A `API.CreditCard` whether it's a Credit Card Information or a Tokenized Credit Card.
        - orderResponseData: A `API.BaseOrderResponseData` object with the response from the Create Order Process.
     
     - Returns: A `API.CheckoutRequestData` object with the required information.
     
     - Throws: `GreenPayError` when the SDK is not initialized or configured properly.
     */
    private func createCheckoutRequestData(creditCard: API.CreditCard, orderResponseData: API.BaseOrderResponseData) throws -> API.CheckoutRequestData{
    
        try self.checkGreenPaySDKInit();
        
        return GreenPayProcess.createCheckoutRequestData(cardData: creditCard, orderResponseData: orderResponseData, greenPayPublicKey: (self.greenPayConfig?.greenPayPublicKey)!);
    }
    
    //MARK: - Tokenized Card
    
    /**
     Creates a new order for tokenizing a new Credit Card.
     
     - Parameters:
        - greenPayTokenizeCardOrderInfo: A `GreenPayTokenizeCardOrderInfo` object to create a new Tokenize Card Order.
     
     - Returns: A Promise with a `GreenPayOrderResult` containing the result of the Tokenize Card Order Process.
     */
    public func createNewTokenizeCardGreenPayOrder(greenPayTokenizeCardOrderInfo: GreenPayTokenizeCardOrderInfo) -> Promise<GreenPayOrderResult>{
        
        return Promise {
            seal in
            
            do{
                try self.checkGreenPaySDKInit();
            } catch {
                seal.reject(error)
            }
            
            let tokenizeCardOrderRequestData: API.TokenizeCardOrderRequestData = API.TokenizeCardOrderRequestData();
            tokenizeCardOrderRequestData.requestId = greenPayTokenizeCardOrderInfo.requestId;
            tokenizeCardOrderRequestData.merchantId = (self.greenPayConfig?.greenPayMerchantId)!;
            tokenizeCardOrderRequestData.secret = (self.greenPayConfig?.greenPaySecret)!;
            
            GreenPayAPI.shared.tokenizeCardCreateOrder(orderRequestData: tokenizeCardOrderRequestData).done({ (tokenizeCardOrderResponseData) in
                
                var orderResult = GreenPayOrderResult();
                
                var securityInfo: GreenPayOrderSecurityInfo = GreenPayOrderSecurityInfo();
                securityInfo.session = tokenizeCardOrderResponseData.session;
                securityInfo.token = tokenizeCardOrderResponseData.token;
                
                orderResult.securityInfo = securityInfo;
                orderResult.orderInfo = greenPayTokenizeCardOrderInfo;
                
                seal.fulfill(orderResult);
                
            }).catch({ (error) in
                seal.reject(error);
            })
        }
    }
    
    /**
     Checkouts an existing order for tokenizing a new Credit Card.
     
     - Parameters:
        - greenPayCreditCard: The `GreenPayCreditCard` to tokenize.
     
     - Returns: A Promise with a `GreenPayTokenizeCardCheckoutOrderResult` containing the result of the Tokenize Card Checkout Process.
     */
    public func checkoutExistingTokenizeCardOrder(greenPayCreditCard: GreenPayCreditCard, greenPayOrderResult: GreenPayOrderResult) -> Promise<GreenPayTokenizeCardCheckoutOrderResult>{
        
        return Promise {
            seal in
            
            do {
                try self.checkGreenPaySDKInit();
            } catch {
                seal.reject(error)
            }
            
            let cardData : API.CardData = API.CardData();
            var cardInfo: API.CardInfo = API.CardInfo();
            var cardExpirationDate: API.CardExpirationDate = API.CardExpirationDate();
            
            cardExpirationDate.month = greenPayCreditCard.expirationMonth;
            cardExpirationDate.year = greenPayCreditCard.expirationYear;
            cardInfo.cardExpirationDate = cardExpirationDate;
            cardInfo.cardHolder = greenPayCreditCard.cardHolder;
            cardInfo.cardNumber = greenPayCreditCard.cardNumber;
            cardInfo.cvc = greenPayCreditCard.cvc;
            cardInfo.nickname = greenPayCreditCard.nickname;
            
            cardData.cardInfo = cardInfo;
            
            let tokenizeaCardOrderResponseData : API.TokenizeCardOrderResponseData = API.TokenizeCardOrderResponseData();
            tokenizeaCardOrderResponseData.session = greenPayOrderResult.securityInfo.session;
            tokenizeaCardOrderResponseData.token = greenPayOrderResult.securityInfo.token;
            
            let checkoutRequestData : API.CheckoutRequestData = try createCheckoutRequestData (creditCard: cardData, orderResponseData: tokenizeaCardOrderResponseData);
            let greenPayTokenizeCardOrderInfo: GreenPayTokenizeCardOrderInfo = greenPayOrderResult.orderInfo as! GreenPayTokenizeCardOrderInfo;
            
            GreenPayAPI.shared.tokenizeCardCheckoutOrder(tokenizeCardOrderResponseData: tokenizeaCardOrderResponseData, checkoutRequestData: checkoutRequestData).done({ (tokenizeCardCheckoutResponseData) in
                
                let transactionVerified = GreenPayProcess.verifyTokenizeCardCheckoutResponse(checkoutResponseData: tokenizeCardCheckoutResponseData, requestId: greenPayTokenizeCardOrderInfo.requestId, greenPayPublicKey: (self.greenPayConfig?.greenPayPublicKey)!);
                if (!transactionVerified){
                    seal.reject(GreenPayError.greenPayError(code: 1, message: NSLocalizedString("ERROR_TRANSACTION_NOT_VALIDATED", comment: "")))
                }
                
                var greenPayCheckoutOrderResult: GreenPayTokenizeCardCheckoutOrderResult = GreenPayTokenizeCardCheckoutOrderResult();
                greenPayCheckoutOrderResult.bin = tokenizeCardCheckoutResponseData.result.bin;
                greenPayCheckoutOrderResult.brand = tokenizeCardCheckoutResponseData.brand;
                
                if let callback = tokenizeCardCheckoutResponseData.callback{
                    greenPayCheckoutOrderResult.callback = callback
                }else{
                    greenPayCheckoutOrderResult.callback = "";
                }
                
                greenPayCheckoutOrderResult.expirationDate = tokenizeCardCheckoutResponseData.expirationDate;
                greenPayCheckoutOrderResult.lastDigits = tokenizeCardCheckoutResponseData.result.lastDigits;
                greenPayCheckoutOrderResult.nickname = tokenizeCardCheckoutResponseData.nickname;
                greenPayCheckoutOrderResult.signature = tokenizeCardCheckoutResponseData.signature;
                greenPayCheckoutOrderResult.status = tokenizeCardCheckoutResponseData.status;
                greenPayCheckoutOrderResult.token = tokenizeCardCheckoutResponseData.result.token;
                
                seal.fulfill(greenPayCheckoutOrderResult);
                
            }).catch({ (error) in
                seal.reject(error);
            })
            
        }
    }
}
