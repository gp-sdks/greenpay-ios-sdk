//
//  KountUtils.swift
//  GreenPay
//
//  Created by Jesús Quirós on 1/14/20.
//  Copyright © 2020 GreenPay. All rights reserved.
//

import Foundation

public struct KountUtils {
    
    public static let shared = KountUtils()
    
    public func generateSessionID () -> String {
        return formatSessionID(sessionID: UUID().uuidString)
    }
    
    public func formatSessionID (sessionID: String) -> String {
        return sessionID.replacingOccurrences(of: "-", with: "");
    }
    
}
