//
//  GreenPayProcess.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation
import CryptoSwift
import SwiftyRSA

class GreenPayProcess {
    
    /**
     Nombre: createCheckoutRequestData
     Descripción: Realiza los cálculos y procesamiento de datos para generar el objeto CheckoutRequestData, requerido para hacer el proceso de Checkout en Greenpay.
     Retorna: El objeto CheckoutRequestData que contiene todos los datos requeridos para realizar el proces de Checkout en Greenpay.
     Parámetros: - cardData: Objeto con los datos de la tarjeta con la que se desea pagar.
     - orderResponseData: Objeto que contiene la respuesta de la creación de la orden en Greenpay.
     */
    static func createCheckoutRequestData(cardData : API.CreditCard, orderResponseData : API.BaseOrderResponseData, greenPayPublicKey : String) -> API.CheckoutRequestData{
        
        // 1. Generar la llave para el cifrado AES en modo CTR
        let aesKey : API.AESKey = API.CipherUtils.generateAESKey();
        let keyBytes : [UInt8] = aesKey.getKeyValue();
        
        // 2. Generar el contador (Counter) para el cifrado AES en modo CTR
        let aesCounter : API.AESCounter = API.CipherUtils.generateAESCounter();
        let counterBytes : [UInt8] = aesCounter.getCounterValue();
        
        // 3. Convertir los datos del objeto que contiene la tarjeta a string y luego a bytes
        let jsonEncoder = JSONEncoder();
        let cardDataEncoded = try? jsonEncoder.encode(cardData);
        
        // 5.1 //  Utilizar AES en modo CTR para cifrar los datos de la tarjeta. Esta implementación utiliza la librería CrytoSwift para realizar el cifrado:
        //  - La llave AES generada representa en un arreglo de bytes.
        //  - El contador utilizado para el modo CTR representado en un arreglo de bytes.
        //  - No se debe usar padding.
        
        var aesHexString = API.AESUtils.cipherWithAESCTR(plainTextAsByteArray: [UInt8](cardDataEncoded!), key: keyBytes, counter: counterBytes)
        
        // 6.   Obtener el valor LK (Liszt Key) requerido para la trama de Checkout.
        // 6.1  Se deben guardar los valores de la llave (como un arreglo de bytes) y el counter (como un valor nunérico entero).
        // 6.2  Al serializado los dos valores se obtiene algo como:
        //                      "{\"k\":[98,191,20,0,70,53,133,100,8,131,110,91,79,186,218,166],\"s\":138}"
        //      En donde el valor "k" contiene la llave AES y el valor "s" contiene el counter usados para cifrar AES en modo CTR del paso 5.
        // 6.3  Utilizar el cifrado asimétrico RSA con la llave pública brindada por Greenpay para cifrar la trama de 6.2
        //      El resultado se encuentra representado en base64.
        var aesPair : API.AESPair = API.AESPair();
        aesPair.key = aesKey.key;
        aesPair.counter = aesCounter.counter;
        
        let aesPairEncoded = try? jsonEncoder.encode(aesPair);
        let aesPairStr = String(data: aesPairEncoded!, encoding: .utf8);
        
        var base64String = API.CipherUtils.cipherWithPublickKey(textToCipher: aesPairStr!, publicKeyContent: greenPayPublicKey);
        
        var checkoutRequestData : API.CheckoutRequestData = API.CheckoutRequestData();
        checkoutRequestData.session = orderResponseData.session;
        checkoutRequestData.lk = base64String;
        checkoutRequestData.ld = aesHexString;
        
        return checkoutRequestData;
    }

    /**
     Nombre: verifyCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout en Greenpay que contiene la firma a validar.
     */
    static func verifyCheckoutResponse(checkoutResponseData : API.CheckoutResponseData, greenPayPublicKey : String) -> Bool{
        
        let verifyValue = "status:" + String(checkoutResponseData.status) + ",orderId:" + String(checkoutResponseData.orderId);
        
        let isSuccessful = API.CipherUtils.verifySignature(valueToVerify: verifyValue, referenceValue: checkoutResponseData.signature, publicKeyStr: greenPayPublicKey);
        
        return isSuccessful;
    }
    
    /**
     Nombre: verifyTokenizeCardCheckoutResponse
     Descripción: Valida que la respuesta obtenida por el proceso de Checkout de tokenización de tarjeta de Greenpay fue generada en los servidores de Greenpay y no ha sido manipulada por terceras partes.
     Retorna: True si la firma de la respuesta del proceso de Checkout de tokenización de tarjeta en Greenpay es correcta. Falso en caso contrario.
     Parámetros: - checkoutResponseData: Objeto con la respuesta del proceso de Checkout de tokenización de tarjeta en Greenpay que contiene la firma a validar.
     */
    static func verifyTokenizeCardCheckoutResponse(checkoutResponseData : API.TokenizeCardCheckoutResponseData, requestId : String, greenPayPublicKey : String) -> Bool{
        
        let verifyValue = "status:" + String(checkoutResponseData.status) + ",requestId:" + requestId;
        
        let isSuccessful = API.CipherUtils.verifySignature(valueToVerify: verifyValue, referenceValue: checkoutResponseData.signature, publicKeyStr: greenPayPublicKey);
        
        return isSuccessful;
    }
}
