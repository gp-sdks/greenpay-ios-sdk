//
//  GreenPayLogger.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Class for logging information in the GreenPaySDK.
class GreenPayLogger: NSObject {
    
    /// Determines whether the logs are enabled or no.
    static var logsEnabled: Bool = false;
    
    /**
     Logs a debug message in the console (if logging currently enabled).
     
     - Parameters:
        - message: The message to log in the console.
     */
    static func logDebug(message: String){
        if (logsEnabled){
            print (message);
        }
    }
    
    /**
     Logs a info message in the console (if logging currently enabled).
     
     - Parameters:
        - message: The message to log in the console.
     */
    static func logInfo(message: String){
        if (logsEnabled){
            print (message);
        }
    }
    
    /**
     Logs a error message in the console (if logging currently enabled).
     
     - Parameters:
        - message: The message to log in the console.
     */
    static func logError(message: String){
        if (logsEnabled){
            print (message);
        }
    }
}
