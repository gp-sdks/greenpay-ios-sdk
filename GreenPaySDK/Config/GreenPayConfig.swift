//
//  GreenPayConfig.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// GreenPay SDK Configuration class. Here you can configure all the values the GreenPay team has created for your account.
public class GreenPayConfig: NSObject {
    
    /// Secret value provided by the GreenPay team.
    public var greenPaySecret : String = "";
    /// MerchantId value provided by the GreenPay team.
    public var greenPayMerchantId : String = "";
    /// Terminal value provided by the GreenPay team.
    public var greenPayTerminal : String = "";
    /// Public Key value provided by the GreenPay team.
    public var greenPayPublicKey : String = "";
    
    
    /**
     Creates a new configuration object.
     
     - Parameters:
        - greenPaySecret: Secret value provided by the GreenPay team.
        - greenPayMerchantId: MerchantId value provided by the GreenPay team.
        - greenPayTerminal: Terminal value provided by the GreenPay team.
        - greenPayPublickKey: Public Key value provided by the GreenPay team.
     */
    public init(greenPaySecret: String, greenPayMerchantId: String, greenPayTerminal: String, greenPayPublickKey: String){
        self.greenPaySecret = greenPaySecret;
        self.greenPayMerchantId = greenPayMerchantId;
        self.greenPayTerminal = greenPayTerminal;
        self.greenPayPublicKey = greenPayPublickKey;
    }
}
