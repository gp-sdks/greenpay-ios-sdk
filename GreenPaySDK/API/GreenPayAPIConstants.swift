//
//  GreenPayAPIConstants.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Constants to use in the GreenPay API.
struct GreenPayAPIConstants {
    
    /// Information about GreenPay URLs
    struct URL{
        
        static private let TOKENIZE_URL: String = "/tokenize";
        
        /// Sandbox URLs
        struct SANDBOX{
            static let CREATE_ORDER: String = "https://sandbox-merchant.greenpay.me";
            static let CHECKOUT_ORDER: String = "https://sandbox-checkout.greenpay.me/kount";
            static let TOKENIZE_CARD_CREATE_ORDER: String = "https://sandbox-merchant.greenpay.me" + URL.TOKENIZE_URL;
            static let TOKENIZE_CARD_CHECKOUT_ORDER: String = "https://sandbox-checkout.greenpay.me" + URL.TOKENIZE_URL;
        }
        
        /// Production URLs
        struct PRODUCTION {
            static let CREATE_ORDER: String = "https://merchant.greenpay.me";
            static let CHECKOUT_ORDER: String = "https://checkout.greenpay.me/kount";
            static let TOKENIZE_CARD_CREATE_ORDER: String = "https://merchant.greenpay.me" + URL.TOKENIZE_URL;
            static let TOKENIZE_CARD_CHECKOUT_ORDER: String = "https://checkout.greenpay.me" + URL.TOKENIZE_URL;
        }
    }
    
    /// HTTPS constants used in the GreenPay API.
    struct HTTP{
        static let POST: String = "POST";
        static let GET: String = "GET";
        
        static let CONTENT_TYPE: String = "Content-type";
        static let CONTENT_TYPE_JSON: String = "application/json";
        
        static let ACCEPT: String = "ACCEPT";
        static let ACCEPT_JSON: String = "ACCEPT_JSON";
        
        static let LISZT_TOKEN: String = "liszt-token";
    }
}
