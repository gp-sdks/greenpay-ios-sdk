//
//  GreenPayAPI.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation
import PromiseKit

class GreenPayAPI: NSObject {
    
    public static let shared = GreenPayAPI()
    
    var sandboxModeEnabled: Bool = false;
    
    public func setSandboxMode(sandboxModeEnabled: Bool){
        self.sandboxModeEnabled = sandboxModeEnabled;
    }
    
    /**
     Nombre: createOrder
     Descripción: Realiza el proceso de creación de una nueva orden en la plataforma Greenpay.
     Retorna: El objeto OrderResponseData que contiene la respuesta a la creación de la orden.
     Parámetros: - OrderResponseData: Objeto OrderRequestData con los datos para crear una nueva orden.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    func createOrder(orderRequestData : API.OrderRequestData) -> Promise<API.OrderResponseData>{
        
        return Promise { seal in
            
            var serviceUrl = GreenPayAPIConstants.URL.PRODUCTION.CREATE_ORDER;
            if (self.sandboxModeEnabled){
                serviceUrl = GreenPayAPIConstants.URL.SANDBOX.CREATE_ORDER;
            }
            GreenPayLogger.logDebug(message: "*****Service URL : \(serviceUrl)")
            let url = URL(string: serviceUrl)!
            
            let request = NSMutableURLRequest(url: url)
            
            let jsonEncoder = JSONEncoder()
            if let orderRequestDataBody = try? jsonEncoder.encode(orderRequestData){
                
                request.httpMethod = GreenPayAPIConstants.HTTP.POST
                request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
                request.addValue(GreenPayAPIConstants.HTTP.CONTENT_TYPE_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.CONTENT_TYPE)
                request.httpBody = orderRequestDataBody
                GreenPayLogger.logDebug(message:"*****Create Order Request : \(String(data: orderRequestDataBody, encoding: .utf8)!)")
                
                URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                    guard let _: Data = data,
                        let _: URLResponse = response, error == nil else {
                            seal.reject(error ?? GreenPayError.greenPayError(code: 0, message: (error?.localizedDescription)!));
                        return
                    }
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    GreenPayLogger.logDebug(message: "*****Creat Order Response : \(dataString!)")
                    
                    //JSONSerialization
                    guard let orderResponseData = try? JSONDecoder().decode(API.OrderResponseData.self, from: data!) else{
                        
                        // The response cannot be converted into a API.OrderResponseData object, mostly due because it's an error. Try to parsed it into a API.APIError object.
                        guard let orderResponseDataError = try? JSONDecoder().decode(API.APIError.self, from: data!) else{
                            // The response cannot be parsed into a API.APIError.
                            // Return a generic error
                            seal.reject(GreenPayError.greenPayError(code: 999, message:NSLocalizedString("UNKNOWN_ERROR", comment: "")));
                            return
                        }
                        
                        // Return the API.APIError as an error
                        seal.reject(GreenPayError.greenPayError(code: orderResponseDataError.status, message: orderResponseDataError.errors.first!));
                        return
                    }
                    
                    seal.fulfill(orderResponseData);
                    
                }.resume()
            }
        }
    }
    
    /**
     Nombre: checkout
     Descripción: Realiza el proceso de checkout en la plataforma Greenpay.
     Retorna: El objeto CheckoutResponseData que contiene la respuesta del proceso de Checkout.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
     - orderResponseData: Objeto orderResponseData con los datos para hacer checkout.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    func checkoutOrder(orderResponseData: API.OrderResponseData, checkoutRequestData: API.CheckoutRequestData) -> Promise<API.CheckoutResponseData>{
        
        return Promise { seal in
            
            var serviceUrl = GreenPayAPIConstants.URL.PRODUCTION.CHECKOUT_ORDER;
            if (self.sandboxModeEnabled){
                serviceUrl = GreenPayAPIConstants.URL.SANDBOX.CHECKOUT_ORDER;
            }
            let url = URL(string: serviceUrl)!
        
            let request = NSMutableURLRequest(url: url)

            
            let jsonEncoder = JSONEncoder()
            if let checkoutRequestDataBody = try? jsonEncoder.encode(checkoutRequestData){
                
                request.httpMethod = GreenPayAPIConstants.HTTP.POST
                request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
                request.addValue(GreenPayAPIConstants.HTTP.CONTENT_TYPE_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.CONTENT_TYPE)
                request.addValue(GreenPayAPIConstants.HTTP.ACCEPT_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.ACCEPT)
                request.addValue(orderResponseData.token, forHTTPHeaderField: GreenPayAPIConstants.HTTP.LISZT_TOKEN)
                
                request.httpBody = checkoutRequestDataBody
                GreenPayLogger.logDebug(message: "*****Checkout Order Request : \(String(data: checkoutRequestDataBody, encoding: .utf8)!)")
                URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                    guard let _: Data = data,
                        let _: URLResponse = response, error == nil else {
                            seal.reject(error ?? GreenPayError.greenPayError(code: 0, message: (error?.localizedDescription)!));
                            return
                    }
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    GreenPayLogger.logDebug(message: "*****Checkout Response : \(dataString!)")
                    
                    //JSONSerialization
                    guard let checkoutResponseData = try? JSONDecoder().decode(API.CheckoutResponseData.self, from: data!) else{
                        
                        // The response cannot be converted into a API.CheckoutResponseData object, mostly due because it's an error. Try to parsed it into a API.APIError object.
                        guard let orderResponseDataError = try? JSONDecoder().decode(API.APIError.self, from: data!) else{
                            // The response cannot be parsed into a API.APIError.
                            // Return a generic error
                            seal.reject(GreenPayError.greenPayError(code: 999, message:NSLocalizedString("UNKNOWN_ERROR", comment: "")));
                            return
                        }
                        
                        // Return the API.APIError as an error
                        seal.reject(GreenPayError.greenPayError(code: orderResponseDataError.status, message: orderResponseDataError.errors.first!));
                        return
                    }
                    
                    seal.fulfill(checkoutResponseData);
                    
                }.resume()
            }
        }
    }
    
    /**
     Nombre: tokenizeCardCreateOrder
     Descripción: Realiza el proceso de creación de una nueva orden para tokenizar tarjeta en la plataforma Greenpay.
     Retorna: El objeto TokenizeCardOrderResponseData que contiene la respuesta a la creación de la orden para tokenizar tarjeta.
     Parámetros: - orderRequestData: Objeto TokenizeCardOrderRequestData con los datos para crear una nueva orden de tokenización de tarjeta.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    func tokenizeCardCreateOrder(orderRequestData : API.TokenizeCardOrderRequestData) -> Promise<API.TokenizeCardOrderResponseData>{
        
        return Promise { seal in
        
            var serviceUrl = GreenPayAPIConstants.URL.PRODUCTION.TOKENIZE_CARD_CREATE_ORDER;
            if (self.sandboxModeEnabled){
                serviceUrl = GreenPayAPIConstants.URL.SANDBOX.TOKENIZE_CARD_CREATE_ORDER;
            }
            let url = URL(string: serviceUrl)!
            
            let request = NSMutableURLRequest(url: url)
            
            let jsonEncoder = JSONEncoder()
            if let orderRequestDataBody = try? jsonEncoder.encode(orderRequestData){
                
                request.httpMethod = GreenPayAPIConstants.HTTP.POST
                request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
                request.addValue(GreenPayAPIConstants.HTTP.CONTENT_TYPE_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.CONTENT_TYPE)
                request.httpBody = orderRequestDataBody
                GreenPayLogger.logDebug(message: "*****Tokenize Card Create Order Request : \(String(data: orderRequestDataBody, encoding: .utf8)!)")
                let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                    guard let _: Data = data,
                        let _: URLResponse = response, error == nil else {
                            seal.reject(error ?? GreenPayError.greenPayError(code: 0, message: (error?.localizedDescription)!));
                            return
                    }
                    
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    GreenPayLogger.logDebug(message: "*****Tokenize Card Create Order Response : \(dataString!)")
                    
                    //JSONSerialization
                    guard let orderResponseData = try? JSONDecoder().decode(API.TokenizeCardOrderResponseData.self, from: data!) else {
                        
                        // The response cannot be converted into a API.TokenizeCardOrderResponseData object, mostly due because it's an error. Try to parsed it into a API.APIError object.
                        guard let orderResponseDataError = try? JSONDecoder().decode(API.APIError.self, from: data!) else{
                            // The response cannot be parsed into a API.APIError.
                            // Return a generic error
                            seal.reject(GreenPayError.greenPayError(code: 999, message:NSLocalizedString("UNKNOWN_ERROR", comment: "")));
                            return
                        }
                        
                        // Return the API.APIError as an error
                        seal.reject(GreenPayError.greenPayError(code: orderResponseDataError.status, message: orderResponseDataError.errors.first!));
                        return
                    }
                    
                    seal.fulfill(orderResponseData);
                    
                }.resume()
            }
        }
    }
    
    /**
     Nombre: tokenizeCardCheckoutOrder
     Descripción: Realiza el proceso de checkout para tokenizar tarjetas en la plataforma Greenpay.
     Retorna: El objeto TokenizeCardCheckoutResponseData que contiene la respuesta del proceso de Checkout de tokenización de tarjeta.
     Parámetros: - cardData: Objeto que representa la tarjeta del cliente.
     - orderResponseData: Objeto TokenizeCardOrderResponseData con los datos para hacer checkout de tokenización de tarjeta.
     - completion - Bloque que contiene la función que se ejecuta al terminar de invoccar la función.
     */
    func tokenizeCardCheckoutOrder(tokenizeCardOrderResponseData: API.TokenizeCardOrderResponseData, checkoutRequestData: API.CheckoutRequestData) -> Promise<API.TokenizeCardCheckoutResponseData>{
        
        return Promise { seal in
        
            var serviceUrl = GreenPayAPIConstants.URL.PRODUCTION.TOKENIZE_CARD_CHECKOUT_ORDER;
            if (self.sandboxModeEnabled){
                serviceUrl = GreenPayAPIConstants.URL.SANDBOX.TOKENIZE_CARD_CHECKOUT_ORDER;
            }
            let url = URL(string: serviceUrl)!
            
            let request = NSMutableURLRequest(url: url)
            
            let jsonEncoder = JSONEncoder()
            if let checkoutRequestDataBody = try? jsonEncoder.encode(checkoutRequestData){
                
                request.httpMethod = GreenPayAPIConstants.HTTP.POST
                request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
                request.addValue(GreenPayAPIConstants.HTTP.CONTENT_TYPE_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.CONTENT_TYPE)
                request.addValue(GreenPayAPIConstants.HTTP.ACCEPT_JSON, forHTTPHeaderField: GreenPayAPIConstants.HTTP.ACCEPT)
                request.addValue(tokenizeCardOrderResponseData.token, forHTTPHeaderField: GreenPayAPIConstants.HTTP.LISZT_TOKEN)
                
                request.httpBody = checkoutRequestDataBody
                GreenPayLogger.logDebug(message: "*****Tokenize Card Checkout Order Request : \(String(data: checkoutRequestDataBody, encoding: .utf8)!)")
                URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                    guard let _: Data = data,
                        let _: URLResponse = response, error == nil else {
                            seal.reject(error ?? GreenPayError.greenPayError(code: 0, message: (error?.localizedDescription)!));
                            return
                    }
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    GreenPayLogger.logDebug(message: "*****Tokenize Card Checkout Response : \(dataString!)")
                    
                    //JSONSerialization
                    guard let checkoutResponseData = try? JSONDecoder().decode(API.TokenizeCardCheckoutResponseData.self, from: data!) else {
                        
                        // The response cannot be converted into a API.TokenizeCardCheckoutResponseData object, mostly due because it's an error. Try to parsed it into a API.APIError object.
                        guard let orderResponseDataError = try? JSONDecoder().decode(API.APIError.self, from: data!) else{
                            // The response cannot be parsed into a API.APIError.
                            // Return a generic error
                            seal.reject(GreenPayError.greenPayError(code: 999, message:NSLocalizedString("UNKNOWN_ERROR", comment: "")));
                            return
                        }
                        
                        // Return the API.APIError as an error
                        seal.reject(GreenPayError.greenPayError(code: orderResponseDataError.status, message: orderResponseDataError.errors.first!));
                        return
                    }
                    
                    seal.fulfill(checkoutResponseData);
                    
                }.resume()
            }
        }
    }
}
