//
//  OrderRequestData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class BaseOrderRequestData: Codable{
        var secret : String
        var merchantId : String
        
        private enum CodingKeys: String, CodingKey {
            case secret = "secret"
            case merchantId = "merchantId"
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(secret, forKey: .secret)
            try container.encode(merchantId, forKey: .merchantId)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            secret = try values.decode(String.self, forKey: .secret)
            merchantId = try values.decode(String.self, forKey: .merchantId)
        }
        
        init() {
            secret = "";
            merchantId = "";
        }
    }
}
