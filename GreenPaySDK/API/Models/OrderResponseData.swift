//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class OrderResponseData: BaseOrderResponseData {
        
        override init() {
            super.init();
        }
        
        required init(from decoder: Decoder) throws {
            try super.init(from: decoder);
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder);
        }
    }
}
