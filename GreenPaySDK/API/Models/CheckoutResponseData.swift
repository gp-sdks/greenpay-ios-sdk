//
//  CheckoutResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct CheckoutResponseData: Codable {
        var status : Int
        var orderId : String
        var authorization : String
        var amount : Int
        var currency : String
        var last4 : String
        var brand : String
        var result : CheckoutResponseDataResult
        var signature : String
        
        enum CodingKeys: String, CodingKey {
            case status = "status"
            case orderId = "orderId"
            case authorization = "authorization"
            case amount = "amount"
            case currency = "currency"
            case last4 = "last4"
            case brand = "brand"
            case result = "result"
            case signature = "_signature"
        }
        
        init() {
            status = 0;
            orderId = "";
            authorization = "";
            amount = 0;
            currency = "";
            last4 = "";
            brand = "";
            result = CheckoutResponseDataResult();
            signature = "";
        }
        
        
    }
}
