//
//  AESCounter.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct AESCounter {
        var counter : Int
        
        /**
         Nombre: getCounterValue
         Descripción: Obtiene el valor del Counter como un arreglo de bytes.
         Retorna: el valor del Counter como un arreglo de bytes.
         */
        func getCounterValue () -> [UInt8] {
            var counterBytes : [UInt8] = [UInt8](repeating: 0, count: 16);
            let intBytes = toByteArray(counter);
            var counterReversedIndex = 15;
            for i in (0...intBytes.count-1) {
                counterBytes[counterReversedIndex] = intBytes[i];
                counterReversedIndex = counterReversedIndex - 1;
            }
            
            return counterBytes;
        }
        
        /**
         Nombre: toByteArray
         Descripción: Convierte el valor entero a un arreglo de bytes.
         Retorna: el valor del Counter como un arreglo de bytes.
         */
        func toByteArray<T>(_ value: T) -> [UInt8] {
            var value = value
            return withUnsafeBytes(of: &value) { Array($0) }
        }
    }
}
