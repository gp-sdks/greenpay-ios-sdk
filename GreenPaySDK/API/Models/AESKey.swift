//
//  AESKey.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API{
    struct AESKey {
        var key : [Int]
        
        /**
         Nombre: getKeyValue
         Descripción: Obtiene el valor de la llave como un arreglo de bytes.
         Retorna: el valor de la llave como un arreglo de bytes.
         */
        func getKeyValue () -> [UInt8] {
            var keyBytes : [UInt8] = [UInt8](repeating: 0, count: 16);
            for i in 0...15 {

                let unsigned = UInt8(key[i]) // -> 0-255
                keyBytes[i] = unsigned;
            }
            
            return keyBytes;
        }
    }
}
