//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class TokenizeCardCheckoutResponseDataResult : Codable{
        var token :  String
        var lastDigits :  String
        var bin :  String
        
        enum CodingKeys: String, CodingKey {
            case token = "token"
            case lastDigits = "last_digits"
            case bin = "bin"
        }
        
        init() {
            token = "";
            lastDigits = "";
            bin = "";
        }
    }
}
