//
//  AesPair.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct AESPair: Codable{
        var key : [Int]
        var counter : Int
        
        enum CodingKeys: String, CodingKey {
            case key = "k"
            case counter = "s"
        }
        
        init() {
            key = [Int]();
            counter = 0;
        }
        
    }
}
