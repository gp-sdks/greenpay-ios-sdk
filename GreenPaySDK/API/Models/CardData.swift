//
//  CardData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class CardData: CreditCard {
        var cardInfo : CardInfo
        
        enum CodingKeys: String, CodingKey {
            case cardInfo = "card"
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(cardInfo, forKey: .cardInfo)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cardInfo = try values.decode(CardInfo.self, forKey: .cardInfo)
            
            try super.init(from: decoder);
        }
        
        override init() {
            cardInfo = CardInfo();
            
            super.init();
        }
    }
}
