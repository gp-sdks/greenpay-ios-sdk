//
//  CheckoutResponseDataResult.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct CheckoutResponseDataResult: Codable {
        var timeLocalTran : String
        var systemsTraceAuditNumber : String
        var success :  Bool
        var retrievalRefNum : String
        var respCode : String
        var reservedPrivate4 : String?
        var procCode : String
        var networkInternationId : String
        var mti : String
        var merchantId : Int
        var dateLocalTran : String
        var cardAcceptorTerminalId : String
        var authorizationIdResp : String
        
        enum CodingKeys: String, CodingKey {
            case timeLocalTran = "time_local_tran"
            case systemsTraceAuditNumber = "systems_trace_audit_number"
            case success = "success"
            case retrievalRefNum = "retrieval_ref_num"
            case respCode = "resp_code"
            case reservedPrivate4 = "reserved_private4"
            case procCode = "proc_code"
            case networkInternationId = "network_international_id"
            case mti = "mti"
            case merchantId = "merchant_id"
            case dateLocalTran = "date_local_tran"
            case cardAcceptorTerminalId = "card_acceptor_terminal_id"
            case authorizationIdResp = "authorization_id_resp"
        }
        
        /*init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            timeLocalTran = try values.decode(String.self, forKey: .timeLocalTran)
            systemsTraceAuditNumber = try values.decode(String.self, forKey: .systemsTraceAuditNumber)
            success = try values.decode(String.self, forKey: .success)
            retrievalRefNum = try values.decode(String.self, forKey: .retrievalRefNum)
            respCode = try values.decode(String.self, forKey: .respCode)
            reservedPrivate4 = try values.decode(String.self, forKey: .reservedPrivate4)
            procCode = try values.decode(String.self, forKey: .procCode)
            networkInternationId = try values.decode(String.self, forKey: .networkInternationId)
            mti = try values.decode(String.self, forKey: .mti)
            merchantId = try values.decode(String.self, forKey: .merchantId)
            dateLocalTran = try values.decode(String.self, forKey: .dateLocalTran)
            cardAcceptorTerminalId = try values.decode(String.self, forKey: .cardAcceptorTerminalId)
            authorizationIdResp = try values.decode(String.self, forKey: .authorizationIdResp)
        }*/
        
        /*func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(timeLocalTran, forKey: .timeLocalTran)
            try container.encode(systemsTraceAuditNumber, forKey: .systemsTraceAuditNumber)
            try container.encode(success, forKey: .success)
            try container.encode(retrievalRefNum, forKey: .retrievalRefNum)
            try container.encode(respCode, forKey: .respCode)
            try container.encode(reservedPrivate4, forKey: .reservedPrivate4)
            try container.encode(procCode, forKey: .procCode)
            try container.encode(networkInternationId, forKey: .networkInternationId)
            try container.encode(mti, forKey: .mti)
            try container.encode(merchantId, forKey: .merchantId)
            try container.encode(dateLocalTran, forKey: .dateLocalTran)
            try container.encode(cardAcceptorTerminalId, forKey: .cardAcceptorTerminalId)
            try container.encode(authorizationIdResp, forKey: .authorizationIdResp)
        }*/
        
        init() {
            timeLocalTran = "";
            systemsTraceAuditNumber = "";
            success = false;
            retrievalRefNum = "";
            respCode = "";
            reservedPrivate4 = ""
            procCode = "";
            networkInternationId = "";
            mti = "";
            merchantId = 0;
            dateLocalTran = "";
            cardAcceptorTerminalId = "";
            authorizationIdResp = "";
        }
    }
}
