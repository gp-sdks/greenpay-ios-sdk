//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class TokenizedCard: CreditCard {
        var token :  String
        
        enum CodingKeys: String, CodingKey {
            case token = "token"
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(token, forKey: .token)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            token = try values.decode(String.self, forKey: .token)
            
            try super.init(from: decoder);
        }
        
        override init() {
            token = "";
            
            super.init();
        }
    }
}
