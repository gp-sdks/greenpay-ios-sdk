//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class TokenizeCardOrderRequestData: BaseOrderRequestData {
        var requestId :  String
        
        private enum CodingKeys: String, CodingKey {
            case requestId = "requestId"
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(requestId, forKey: .requestId)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            requestId = try values.decode(String.self, forKey: .requestId)
            
            try super.init(from: decoder);
        }
        
        override init() {
            requestId = "";
            
            super.init();
        }
    }
}
