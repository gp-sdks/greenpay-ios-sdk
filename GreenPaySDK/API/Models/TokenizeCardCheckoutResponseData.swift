//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class TokenizeCardCheckoutResponseData : Codable{
        var status :  Int
        var expirationDate :  String
        var brand :  String
        var nickname :  String
        var callback :  String?
        var result :  TokenizeCardCheckoutResponseDataResult
        var signature :  String
        
        enum CodingKeys: String, CodingKey {
            case status = "status"
            case expirationDate = "expiration_date"
            case brand = "brand"
            case nickname = "nickname"
            case callback = "callback"
            case result = "result"
            case signature = "_signature"
        }
        
        init() {
            status = 0;
            expirationDate = "";
            brand = "";
            nickname = "";
            callback = "";
            result = TokenizeCardCheckoutResponseDataResult();
            signature = "";
        }
        
        func encode(to encoder: Encoder) throws
        {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode (status, forKey: .status)
            try container.encode (expirationDate, forKey: .expirationDate)
            try container.encode (brand, forKey: .brand)
            try container.encode (nickname, forKey: .nickname)
            try container.encode (callback, forKey: .callback)
            try container.encode (result, forKey: .result)
            try container.encode (signature, forKey: .signature)
        }
    }
}
