//
//  CheckoutRequestData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct CheckoutRequestData: Codable {
        var session : String
        var ld : String
        var lk : String
        
        enum CodingKeys: String, CodingKey {
            case session = "session"
            case ld = "ld"
            case lk = "lk"
        }
        
        init() {
            session = "";
            ld = "";
            lk = "";
        }
    }
}
