//
//  OrderResponseData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class BaseOrderResponseData: Codable {
        var session : String
        var token :  String
        
        enum CodingKeys: String, CodingKey {
            case session = "session"
            case token = "token"
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(session, forKey: .session)
            try container.encode(token, forKey: .token)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            session = try values.decode(String.self, forKey: .session)
            token = try values.decode(String.self, forKey: .token)
        }
        
        init() {
            session = "";
            token = "";
        }
    }
}
