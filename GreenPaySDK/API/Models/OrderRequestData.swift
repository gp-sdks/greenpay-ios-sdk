//
//  OrderRequestData.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    class OrderRequestData: BaseOrderRequestData {
        
        var terminal : String
        var amount : Double
        var currency : String
        var description : String
        var orderReference: String
        
        enum CodingKeys: String, CodingKey {
            case terminal = "terminal"
            case amount = "amount"
            case currency = "currency"
            case description = "description"
            case orderReference = "orderReference"
        }
        
        override func encode(to encoder: Encoder) throws {
            try super.encode(to: encoder)
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(terminal, forKey: .terminal)
            try container.encode(amount, forKey: .amount)
            try container.encode(currency, forKey: .currency)
            try container.encode(description, forKey: .description)
            try container.encode(orderReference, forKey: .orderReference)
        }
        
        required init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            terminal = try values.decode(String.self, forKey: .terminal)
            amount = try values.decode(Double.self, forKey: .amount)
            currency = try values.decode(String.self, forKey: .currency)
            description = try values.decode(String.self, forKey: .description)
            orderReference = try values.decode(String.self, forKey: .orderReference)
            
            try super.init(from: decoder);
        }
        
        override init() {
            terminal = "";
            amount = 0;
            currency = "";
            description = "";
            orderReference = "";
            
            super.init();
        }
        
    }
}
