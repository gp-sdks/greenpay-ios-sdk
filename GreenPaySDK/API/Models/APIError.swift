//
//  AesPair.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct APIError: Codable{
        var status : Int
        var errors : [String]
        
        init() {
            status = 0;
            errors = [String]();
        }
        
    }
}
