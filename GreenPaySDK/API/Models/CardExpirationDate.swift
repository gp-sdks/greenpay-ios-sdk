//
//  CardExpirationDate.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct CardExpirationDate: Codable{
        var month : Int
        var year : Int
        
        enum CodingKeys: String, CodingKey {
            case month = "month"
            case year = "year"
        }
        
        init() {
            month = 0;
            year = 0;
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            month = try values.decode(Int.self, forKey: .month)
            year = try values.decode(Int.self, forKey: .year)
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(month, forKey: .month)
            try container.encode(year, forKey: .year)
        }
    }
}
