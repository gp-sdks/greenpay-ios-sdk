//
//  CardInfo.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/27/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API {
    struct CardInfo: Codable {
        var cardHolder : String
        var cardExpirationDate : CardExpirationDate
        var cardNumber : String
        var cvc :  String
        var nickname : String
        
        enum CodingKeys: String, CodingKey {
            case cardHolder = "cardHolder"
            case cardExpirationDate = "expirationDate"
            case cardNumber = "cardNumber"
            case cvc = "cvc"
            case nickname = "nickname"
        }
        
        init() {
            cardHolder = "";
            cardExpirationDate = CardExpirationDate();
            cardNumber = "";
            cvc = "";
            nickname = "";
        }
    }
}
