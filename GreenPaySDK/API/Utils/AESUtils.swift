//
//  AESUtils.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 12/6/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation
import CryptoSwift

extension API{
    class AESUtils {

        /**
         Nombre: cipherWithAESCTR
         Descripción: Realiza un cifrado simétrico AES CTR.
         Retorna: Un String eb formato hexadecimal con el contenido cifrado.
         Parámetros: - plainTextAsByteArray: Contenido a cifrar representado como un arreglo de bytes.
         - key: Llave para cifrado AES representada como un arreglo de bytes.
         - counter: Contador para el cifrado AES CTR como un arreglo de bytes.
         */
        static func cipherWithAESCTR (plainTextAsByteArray: [UInt8], key: [UInt8], counter: [UInt8]) -> String{
            var aesHexString = "";
            do {
                let aes = try AES(key: key, blockMode: CTR(iv: counter), padding: .noPadding);
                let aesCipheredData = try aes.encrypt(plainTextAsByteArray);
                aesHexString = StringUtils.byteArrayToHexString(aesCipheredData);
            } catch {
                //handle error
                print(error)
            }
            return aesHexString;
        }
    }
}
