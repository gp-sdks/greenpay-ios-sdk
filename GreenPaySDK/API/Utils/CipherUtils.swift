//
//  CipherUtils.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 11/29/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation
import SwiftyRSA

extension API{
    class CipherUtils {
        
        /**
         Nombre: generateAESKey
         Descripción: Genera un arreglo de enteros (0-255) aleatorios que funciona como llave privada para cifrado simétrico AES
         Retorna: Un arreglo de números enteros aleatorios que se usan como llave de cifrado.
         */
        static func generateAESKey() -> API.AESKey{
            var key : [Int] = [Int](repeating: 0, count: 16);
            for i in 0...15 {
                key[i] = (Int)(arc4random_uniform(256));
            }
            
            let aesKey : API.AESKey = AESKey(key: key);
            return aesKey;
        }
        
        /**
         Nombre: generateAESCounter
         Descripción: Genera un arreglo de enteros (0-255) aleatorios que funciona como llave privada para cifrado simétrico AES
         Retorna: Un arreglo de números enteros aleatorios que se usan como llave de cifrado.
         */
        static func generateAESCounter() -> API.AESCounter{
            let counter : Int = (Int)(arc4random_uniform(256));
            
            let aesCounter : API.AESCounter = API.AESCounter(counter: counter);
            return aesCounter;
        }
        
        /**
         Nombre: cipherWithPublickKey
         Descripción: Cifra en RSA.
         Retorna: Una cadena en base64 con los datos cifrados con la llave pública.
         Parámetros: - textToCipher: Texto a cifrar.
         - publicKeyContent: Llave pública en formato String.
         */
        static func cipherWithPublickKey(textToCipher: String, publicKeyContent: String) -> String{
            let publicKey = try PublicKey.publicKeys(pemEncoded: publicKeyContent);
            let clear = try? ClearMessage(string: textToCipher, using: .utf8)
            let rsaEncrypted = try? clear!.encrypted(with: publicKey[0], padding: .PKCS1)
            
            let base64String = rsaEncrypted!.base64String
            
            return base64String;
        }
        
        /**
         Nombre: verifySignature
         Descripción: Valida la firma de una transacción usando SHA256withRSA.
         Retorna: True si la firma es correcta. False en caso contrario.
         Parámetros: - valueToVerify: Valor String a verificar.
         - referenceValue: Valor en String base64 contra el cual comparar la firma
         - publicKeyStr: Llave pública como un String.
         */
        static func verifySignature(valueToVerify: String, referenceValue: String, publicKeyStr : String ) -> Bool{
            
            do{
                let publicKey = try PublicKey.publicKeys(pemEncoded: publicKeyStr);
                let clear = try ClearMessage(string: valueToVerify, using: .utf8)
                let signatureData = StringUtils.dataWithHexString(hex: referenceValue);
                let signature = try Signature(data: signatureData);
                
                let isSuccessful = try clear.verify(with: publicKey[0], signature: signature, digestType: .sha256)
                
                return isSuccessful;
            }catch let error {
                print (error);
            }
            
            return false;
        }
    }
}
