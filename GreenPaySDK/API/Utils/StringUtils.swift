//
//  StringUtils.swift
//  GreenPay Checkout Example
//
//  Created by Jose Saurez on 12/4/18.
//  Copyright © 2018 GreenPay. All rights reserved.
//

import Foundation

extension API{
    struct StringUtils {
        
        // Caracteres validos en formato HEX
        private static let CHexLookup : [Character] =
            [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" ]

        
        /**
         Nombre: byteArrayToHexString
         Descripción: Convierte un arreglo de bytes en una cadena de caracteres en formato Hexadecimal.
         Retorna: La representación Hexadecimal del arreglo de bytes parámetro.
         */
        public static func byteArrayToHexString(_ byteArray : [UInt8]) -> String {
            
            var stringToReturn = ""
            
            for oneByte in byteArray {
                let asInt = Int(oneByte)
                stringToReturn.append(StringUtils.CHexLookup[asInt >> 4])
                stringToReturn.append(StringUtils.CHexLookup[asInt & 0x0f])
            }
            return stringToReturn
        }
        
        /**
         Nombre: dataWithHexString
         Descripción: Convierte una cadena de caracteres en formato Hexadecimal en su representación como objeto de datos en Swift.
         Retorna: La representación como un objeto de datos de la cadena Hexadecimal parámetro.
         */
        public static func dataWithHexString(hex: String) -> Data {
            var hex = hex
            var data = Data()
            while(hex.count > 0) {
                let subIndex = hex.index(hex.startIndex, offsetBy: 2)
                let c = String(hex[..<subIndex])
                hex = String(hex[subIndex...])
                var ch: UInt32 = 0
                Scanner(string: c).scanHexInt32(&ch)
                var char = UInt8(ch)
                data.append(&char, count: 1)
            }
            return data
        }
    }
}
