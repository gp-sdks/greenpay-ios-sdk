//
//  GreenPayException.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/*public class GreenPayError : Error {
    let message: String
    
    init(_ message: String) {
        self.message = message
    }
    
    public var localizedDescription: String {
        return message
    }
}*/

/// Error descriptor for GreenPay
public enum GreenPayError: Error {
    /// A Congfiguration-based error
    case configurationError (code: Int, message: String)
    
    /// A transaction has not been correctly validated
    case transactionValidationError (code: Int, message: String)
    
    /// A General GreenPay error
    case greenPayError (code: Int, message: String)
}
