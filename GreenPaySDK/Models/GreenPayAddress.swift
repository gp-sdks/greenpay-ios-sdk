//
//  GreenPayAddress.swift
//  GreenPay
//
//  Created by Jesús Quirós on 1/13/20.
//  Copyright © 2020 GreenPay. All rights reserved.
//

import Foundation

public struct GreenPayAddress {
    
    public var country: String;
    public var province: String?;
    public var city: String?;
    public var street1: String;
    public var street2: String?;
    public var zip: String?;
    
    public init (country: String, street1: String) {
        self.country = country;
        self.street1 = street1;
        self.province = nil;
        self.city = nil;
        self.street2 = nil;
        self.zip = nil;
    }
    
    public init (country: String, province: String? = nil, city: String? = nil, street1: String, street2: String? = nil, zip: String? = nil){
        self.country = country;
        self.province = province;
        self.city = city;
        self.street1 = street1;
        self.street2 = street2;
        self.zip = zip;
    }
    
}
