//
//  GreenPayTokenizeCardOrderInfo.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Order information for the Tokenize Credit Card Process.
public class GreenPayTokenizeCardOrderInfo : GreenPayBaseOrderInfo{
    /// Request Id to assign the Tokenize Card Order Information.
    public var requestId : String
    
    /**
     Default init for GreenPayTokenizeCardOrderInfo
    */
    public override init() {
        requestId = "";
    }
    
    /**
     Generates and sets a random Request Id for the Tokenize Card Order Information.
     If you want to use a custom value, you can use the `requestId` property of the class.
     
     - Parameters:
        - token: The token for the Credit Card.
     */
    public func generateRandomRequestId(){
        let number = Int.random(in: 1 ... 100000)
        self.requestId = String(number);
    }
}
