//
//  GreenPayOrderInfo.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Order Information for the Create Order Process
public class GreenPayOrderInfo : GreenPayBaseOrderInfo{
    
    var customerName: String;
    var customerEmail: String?;
    var shippingAddress: GreenPayAddress;
    var billingAddress: GreenPayAddress;
    var amount : Double;
    var currency : String;
    var description : String;
    var orderReference : String;
    var products: Array<GreenPayProduct>?;
    
    public init(customerName: String, shippingAddress: GreenPayAddress, billingAddress: GreenPayAddress, amount: Double, currency: String, description:String, orderReference: String) {
        self.customerName = customerName;
        self.customerEmail = nil;
        self.shippingAddress = shippingAddress;
        self.billingAddress = billingAddress;
        self.amount = amount;
        self.currency = currency;
        self.description = description;
        self.orderReference = orderReference;
        self.products = nil;
    }
    
    public init(customerName: String, customerEmail: String?, shippingAddress: GreenPayAddress, billingAddress: GreenPayAddress, amount: Double, currency: String, description:String, orderReference: String, products: Array<GreenPayProduct>?) {
        self.customerName = customerName;
        self.customerEmail = customerEmail;
        self.shippingAddress = shippingAddress;
        self.billingAddress = billingAddress;
        self.amount = amount;
        self.currency = currency;
        self.description = description;
        self.orderReference = orderReference;
        self.products = products;
    }
}
