//
//  GreenPayCreditCard.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Information of a Credit Card in GreenPay
public struct GreenPayCreditCard {

    var cardHolder : String;
    var cardNumber : String;
    var expirationMonth : Int;
    var expirationYear : Int;
    var cvc : String;
    var nickname : String;
    
    public init(cardHolder: String, cardNumber: String, expirationMonth: Int, expirationYear: Int, cvc: String, nickname: String) {
        self.cardHolder = cardHolder;
        self.cardNumber = cardNumber;
        self.expirationMonth = expirationMonth;
        self.expirationYear = expirationYear;
        self.cvc = cvc;
        self.nickname = nickname;
    }
}
