//
//  GreenPayProduct.swift
//  GreenPay
//
//  Created by Jesús Quirós on 1/13/20.
//  Copyright © 2020 GreenPay. All rights reserved.
//

import Foundation

public struct GreenPayProduct {
    
    public var description: String;
    public var skuId: String;
    public var quantity: Int;
    public var price: Double;
    public var type: String;
    
    public init (description: String, skuId: String, quantity: Int, price: Double, type: String) {
        self.description = description;
        self.skuId = skuId;
        self.quantity = quantity;
        self.price = price;
        self.type = type;
    }
    
}
