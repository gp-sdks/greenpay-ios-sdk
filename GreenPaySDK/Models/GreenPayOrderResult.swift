//
//  GreenPayOrderResult.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Result for a GreenPay Order Process
public struct GreenPayOrderResult {
    /// Order information sent to GreenPay
    public var orderInfo : GreenPayBaseOrderInfo
    
    /// Security Information of the order result
    public var securityInfo : GreenPayOrderSecurityInfo
    
    /**
     Default init for GreenPayOrderResult
    */
    public init() {
        orderInfo = GreenPayBaseOrderInfo();
        securityInfo = GreenPayOrderSecurityInfo();
    }
}
