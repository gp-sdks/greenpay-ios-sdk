//
//  GreenPayTokenizeCardCheckoutOrderResult.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Result for the Tokenize Card Checkout Process
public struct GreenPayTokenizeCardCheckoutOrderResult {
    /// Status of the Checkout
    public var status : Int
    
    /// Credit Card Expiration Date
    public var expirationDate : String
    
    /// Credit Card Brand
    public var brand : String
    
    ///  Credit Card Nickname
    public var nickname : String
    
    /// Checkout Callback
    public var callback : String
    
    /// Transaction signature for validation
    public var signature : String
    
    /// GreenPay Token generated for the Credit Card
    public var token : String
    
    /// The last 4 digits of the Credit Card
    public var lastDigits : String
    
    /// The Credit Card bin
    public var bin : String
    
    /**
     Default init for GreenPayTokenizeCardCheckoutOrderResult
    */
    public init() {
        status = 0;
        expirationDate = "";
        brand = "";
        nickname = "";
        callback = "";
        signature = "";
        token = "";
        lastDigits = "";
        bin = "";
    }
}
