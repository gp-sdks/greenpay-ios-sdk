//
//  GreenPayCheckoutOrderResult.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Result for the Order Checkout Process
public struct GreenPayCheckoutOrderResult {
    /// Status of the Checkout
    public var status : Int
    
    /// Order ID associated with the checkout response
    public var orderId : String
    
    /// Authorization number of the payment
    public var authorization : String
    
    /// Credit Card last 4 digits
    public var last4 : String
    
    /// Credit Card Brand
    public var brand : String
    
    /// Transaction signature to be validated
    public var signature : String
    
    /**
     Default init for GreenPayCheckoutOrderResult
    */
    public init() {
        status = 0;
        orderId = "";
        authorization = "";
        last4 = "";
        brand = "";
        signature = "";
    }
}
