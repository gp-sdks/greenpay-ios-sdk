//
//  GreenPayOrderSecurityInfo.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// Security information as a result of a GreenPay Order Process.
public struct GreenPayOrderSecurityInfo {
    /// Session associated with the order
    public var session : String
    
    /// Token associated with the order.
    public var token : String
    
    /**
     Default init for GreenPayOrderSecurityInfo
    */
    public init() {
        session = "";
        token = "";
    }
}
