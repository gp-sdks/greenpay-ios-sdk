//
//  GreenPayTokenizedCard.swift
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import Foundation

/// A GreenPay Tokenized Card
public struct GreenPayTokenizedCard {

    public var token : String;
    
    public init(token: String) {
        self.token = token;
    }
    
}
