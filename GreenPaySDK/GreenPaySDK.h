//
//  GreenPaySDK.h
//  GreenPaySDK
//
//  Created by Jose Manuel Suarez Lee on 2/12/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KountDataCollector/KDataCollector.h"

//! Project version number for GreenPaySDK.
FOUNDATION_EXPORT double GreenPaySDKVersionNumber;

//! Project version string for GreenPaySDK.
FOUNDATION_EXPORT const unsigned char GreenPaySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GreenPaySDK/PublicHeader.h>
