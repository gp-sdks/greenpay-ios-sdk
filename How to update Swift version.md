# ****GreenPay iOS SDK Swift Version Management****

## How to upgrade Swift versions for the iOS SDK

* Update the Swift version in the Build Settings of the SDK Project/Target.
* Update each Pod version in the pods file (Podfile) for a version that supports the new Swift version (in case of need).
* Make a pod install or update to use the latests pods.
* Build the SDK framework.
* Update the .podspec file to update the versions of the pods updated in the project.
* Update the .podspec file to update the new Swift version in the attribute spec.swift_version.
* Release a new version of the SDK (both to the public git repo with the .framework file and also push the updated .podspec to Cocoapods.
