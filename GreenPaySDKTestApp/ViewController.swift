//
//  ViewController.swift
//  GreenPaySDKTestApp
//
//  Created by Jose Manuel Suarez Lee on 2/15/19.
//  Copyright © 2019 GreenPay. All rights reserved.
//

import UIKit
import GreenPay

class ViewController: UIViewController {
    
    @IBOutlet weak var checkoutWithTokenizedCardButton: UIButton!
    
    var gpOrderInfo: GreenPayOrderInfo = GreenPayOrderInfo(customerName: "John Doe", shippingAddress: GreenPayAddress(country: "CR", street1: "SJ"), billingAddress: GreenPayAddress(country: "CR", street1: "SJ"), amount: 100.0, currency: "CRC", description: "desc", orderReference: "1");
    
    var gpCreditCard : GreenPayCreditCard = GreenPayCreditCard(cardHolder: "Jhon Doe", cardNumber: "4795736054664396", expirationMonth: 9, expirationYear: 21, cvc: "123", nickname: "visa4396");
    
    var gpTokenizedCreditCard : GreenPayTokenizedCard? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkoutWithTokenizedCardButton.isEnabled = false;
        
        let greenPaySecret = "MTZDMjVBODVBNUREODBGRjE3OTI1NjEzNDZDMEQ5ODgyNzlCN0ZEMDI5RkJDNkFENDMyRjJENEFDOEFFMzAzQzYzNkYyNjRDQUZERTI3OUFGNkQ3NjAzRTA3MENCNEJEMDZGODlGRUQzNzNGQzZGNEU3NjAxRjVDQ0EwRDREQTI=";
        let greenPayMerchantId = "278d4b8f-d6cb-41ea-bffa-9760a3556079";
        let greenPayTerminal = "Freelance-BNCR-Colones";
        let greenPayPublicKey = "-----BEGIN PUBLIC KEY-----\n" +
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQGA+x/Z7WMLuQU2gawuJ4u9lP\n" +
            "0XBBphHKbmYxnqzfnXGuRWfLred588td37zv95p2dh6fcuII5w3G+KXBNV2DVhwa\n" +
            "N3qG9ihoFKaHtFlQApFsLK1DO1G0KZ6rhBn6Bny5aWCS8I1Hq0KuVzGHbjKoLVwO\n" +
            "/IBIfLn7lWgVcGzh+QIDAQAB\n" +
        "-----END PUBLIC KEY-----";
        
        let config: GreenPayConfig = GreenPayConfig(greenPaySecret: greenPaySecret, greenPayMerchantId: greenPayMerchantId, greenPayTerminal: greenPayTerminal, greenPayPublickKey: greenPayPublicKey);
        
        GreenPay.shared.initGreenPaySDK(config: config);
        GreenPay.shared.enableSandboxMode(sandboxModeEnabled: true);
        GreenPay.shared.enableLogs(enableLogs: true);
    }

    
    @IBAction func checkoutButtonTouchupInside(_ sender: Any) {
        GreenPay.shared.createNewGreenPayOrder(greenPayOrderInfo: self.gpOrderInfo).done { (greenPayOrderResult) in
            print("createNewGreenPayOrder Success: Session: " + greenPayOrderResult.securityInfo.session + " , Token: " + greenPayOrderResult.securityInfo.token);
            GreenPay.shared.checkoutExistingOrder(greenPayCreditCard: self.gpCreditCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in
                print("checkoutExistingOrder Success ");
            }).catch({ (error) in
                print("checkoutExistingOrder Error: ");
            });
        }.catch({(error) in
            print("createNewGreenPayOrder Error");
        })
    }
    
    @IBAction func tokenizerCardButtonTouchUpInside(_ sender: Any) {
        
        let greenPayTokenizeCardOrderInfo: GreenPayTokenizeCardOrderInfo = GreenPayTokenizeCardOrderInfo();
        
        greenPayTokenizeCardOrderInfo.generateRandomRequestId();
        
        GreenPay.shared.createNewTokenizeCardGreenPayOrder(greenPayTokenizeCardOrderInfo: greenPayTokenizeCardOrderInfo).done { (greenPayOrderResult) in
            
            print("createNewTokenizeCardGreenPayOrder Success: Session: " + greenPayOrderResult.securityInfo.session + " , Token: " + greenPayOrderResult.securityInfo.token);
            
            GreenPay.shared.checkoutExistingTokenizeCardOrder(greenPayCreditCard: self.gpCreditCard, greenPayOrderResult: greenPayOrderResult).done({ (greenPayTokenizeCardCheckoutOrderResult) in
            
                self.gpTokenizedCreditCard = GreenPayTokenizedCard(token: greenPayTokenizeCardCheckoutOrderResult.token);
                
                print("checkoutExistingTokenizeCardOrder Success: Token: " + greenPayTokenizeCardCheckoutOrderResult.token);
                
                self.checkoutWithTokenizedCardButton.isEnabled = true;
                
            }).catch({ (error) in
                print("checkoutExistingTokenizeCardOrder Error: ", error.localizedDescription);
            });
            
        }.catch { (error) in
            print("createNewTokenizeCardGreenPayOrder Error: " , error.localizedDescription);
        }
    }
    
    
    @IBAction func checkoutWithTokenizedCard(_ sender: Any) {
        GreenPay.shared.createNewGreenPayOrder(greenPayOrderInfo: self.gpOrderInfo).done { (greenPayOrderResult) in
            print("createNewGreenPayOrder Success: Session: " + greenPayOrderResult.securityInfo.session + " , Token: " + greenPayOrderResult.securityInfo.token);
            GreenPay.shared.checkoutExistingOrder(greenPayTokenizedCard: self.gpTokenizedCreditCard!, greenPayOrderResult: greenPayOrderResult).done({ (greenPayCheckoutOrderResult) in
                print("checkoutExistingOrder Success");
            }).catch({ (error) in
                print("checkoutExistingOrder Error: ");
            });
        }.catch({error in
            print("error createNewGreenPayOrder")
        });
    }
}

